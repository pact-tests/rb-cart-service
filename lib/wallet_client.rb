require 'httparty'
require 'json'

class WalletClient
  include HTTParty

  base_uri 'http://localhost'

  def charge(amount, user_id)
    self.class.put(
      "/api/wallets/#{user_id}", 
      headers: { 'Content-Type' => 'application/json' },
      body: { coins: -amount }.to_json
    )
    :charged
  end
end
