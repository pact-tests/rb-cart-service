# Run Test

```
bundle exec rspec ./spec/service_providers/wallet_client_spec.rb
```

A new contract will be availabe in the folder `spec/pacts`.

# Publish a new contract

```
bundle exec rake pact:publish 
```
