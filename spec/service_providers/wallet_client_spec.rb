require 'pact/consumer/rspec'
require_relative '../../lib/wallet_client'

describe WalletClient, pact: true do
  WALLER_SERVICE_PORT = 1234

  before { WalletClient.base_uri("localhost:#{WALLER_SERVICE_PORT}") }

end
